from queue import PriorityQueue
from pyamaze import maze, agent, textLabel


def manhattanDist(cell1, cell2):
    x1, y1 = cell1
    x2, y2 = cell2áass

    return abs(x1 - x2) + abs(y1 - y2)


def aStar(m):
    start = (m.rows, m.cols)
    g_score = {cell: float('inf') for cell in m.grid}
    g_score[start] = 0
    f_score = {cell: float('inf') for cell in m.grid}
    f_score[start] = manhattanDist(start, (1, 1))

    open = PriorityQueue()
    open.put((manhattanDist(start, (1, 1)), manhattanDist(start, (1, 1)), start))
    aPath = {}
    while not open.empty():
        currCell = open.get()[2]
        if currCell == (1, 1):
            break
        for d in 'EWNS':
            if m.maze_map[currCell][d] == True:
                childCell = None
                if d == 'E':
                    childCell = (currCell[0], currCell[1] + 1)
                if d == 'W':
                    childCell = (currCell[0], currCell[1] - 1)
                if d == 'N':
                    childCell = (currCell[0] - 1, currCell[1])
                if d == 'S':
                    childCell = (currCell[0] + 1, currCell[1])

                temp_g_score = g_score[currCell] + 1
                temp_f_score = temp_g_score + manhattanDist(childCell, (1, 1))

                if temp_f_score < f_score[childCell]:
                    g_score[childCell] = temp_g_score
                    f_score[childCell] = temp_f_score
                    open.put((temp_f_score, manhattanDist(childCell, (1, 1)), childCell))
                    aPath[childCell] = currCell

    fwdPath = {}
    cell = (1, 1)
    while cell != start:
        fwdPath[aPath[cell]] = cell
        cell = aPath[cell]

    return fwdPath


if __name__ == '__main__':
    # region sample using pyamaze to generate a random grid with obstacles
    # grid_direction_structure used by
    pyamaze = {
        (1, 1): {'E': 1, 'W': 0, 'N': 0, 'S': 0}, (2, 1): {'E': 0, 'W': 0, 'N': 0, 'S': 1},
        (3, 1): {'E': 1, 'W': 0, 'N': 1, 'S': 0}, (1, 2): {'E': 0, 'W': 1, 'N': 0, 'S': 1},
        (2, 2): {'E': 0, 'W': 0, 'N': 1, 'S': 1}, (3, 2): {'E': 1, 'W': 1, 'N': 1, 'S': 0},
        (1, 3): {'E': 0, 'W': 0, 'N': 0, 'S': 1}, (2, 3): {'E': 0, 'W': 0, 'N': 1, 'S': 1},
        (3, 3): {'E': 0, 'W': 1, 'N': 1, 'S': 0}
    }

    # grid_definition_structure used by pyamaze = [(1, 1), (2, 1), (3, 1), (1, 2), (2, 2), (3, 2), (1, 3), (2, 3), (3, 3)]

    m = maze()
    m.CreateMaze()
    path = aStar(m)
    a = agent(m, footprints=True)
    m.tracePath({a: path})
    l = textLabel(m, 'Path length:', len(path) + 1)

    print(m.maze_map)
    print('---')
    print(path)
    print('***')
    print(len(path) + 1)
    m.run()
    # endregion

    # region TODO sample singapore straits grid (4x4 only), no obstacles
    singapore_straits_grid = {(1, 1): {'lat': 103.594233, 'lon': 1.087313, 'E': 0, 'W': 1, 'N': 1, 'S': 1},
                              (1, 2): {'lat': 103.594233, 'lon': 1.087313, 'E': 1, 'W': 1, 'N': 0, 'S': 1},
                              (1, 3): {'lat': 103.594233, 'lon': 1.293038, 'E': 1, 'W': 1, 'N': 1, 'S': 1},
                              (1, 4): {'lat': 103.594233, 'lon': 1.293038, 'E': 1, 'W': 1, 'N': 0, 'S': 1},
                              (2, 1): {'lat': 103.594233, 'lon': 1.087313, 'E': 1, 'W': 1, 'N': 1, 'S': 1},
                              (2, 2): {'lat': 103.594233, 'lon': 1.087313, 'E': 0, 'W': 1, 'N': 1, 'S': 1},
                              (2, 3): {'lat': 103.594233, 'lon': 1.293038, 'E': 1, 'W': 1, 'N': 1, 'S': 1},
                              (2, 4): {'lat': 103.594233, 'lon': 1.293038, 'E': 0, 'W': 1, 'N': 1, 'S': 1},
                              (3, 1): {'lat': 104.096742, 'lon': 1.087313, 'E': 1, 'W': 1, 'N': 0, 'S': 1},
                              (3, 2): {'lat': 104.096742, 'lon': 1.087313, 'E': 1, 'W': 0, 'N': 1, 'S': 1},
                              (3, 3): {'lat': 104.096742, 'lon': 1.293038, 'E': 1, 'W': 1, 'N': 1, 'S': 1},
                              (3, 4): {'lat': 104.096742, 'lon': 1.293038, 'E': 1, 'W': 0, 'N': 1, 'S': 1},
                              (4, 1): {'lat': 104.096742, 'lon': 1.087313, 'E': 1, 'W': 1, 'N': 1, 'S': 1},
                              (4, 2): {'lat': 104.096742, 'lon': 1.087313, 'E': 1, 'W': 1, 'N': 0, 'S': 1},
                              (4, 3): {'lat': 104.096742, 'lon': 1.293038, 'E': 1, 'W': 0, 'N': 1, 'S': 1},
                              (4, 4): {'lat': 104.096742, 'lon': 1.293038, 'E': 1, 'W': 1, 'N': 1, 'S': 0}}
    # TODO 1 randomise the obstacles in the grid above by setting direction = 0 (closed) eg. (1, 4): {'lat': 103.594233, 'lon': 1.293038, 'E': 1, 'W': 0, 'N': 0, 'S': 0},

    # TODO 2 using the modified singapore_straits_grid structure, amend the aStar algorithm to determine the shortest path between any start and end point. the start and end point are coordinates in lat and long

    # endregion

    # region TODO implement with actual singapore straits grid

    # endregion
